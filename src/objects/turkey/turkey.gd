extends Node2D

var active = false
var hits = 0
var dead = false
@onready var points = preload("res://objects/points/points.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	Global.turkey = self
	position.x=315
	position.y = -315
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if dead:
		return
		
	if position.y < 315 && !active:
		position.y +=10
	else:
		active = true

func hit():
	if $sprite.animation != "default":
		return
	
	if position.y < 315:
		return
	
	if hits < 2:
		$ResetTimer.start(0)
		$sprite.play("hit")
	
	if hits == 0:
		add_points(10)
		$hit.play()
	
	if hits == 1:
		$hit2.play()
		add_points(25)
		
	if hits > 1:
		add_points(50)
		$respawnTimer.start(0)
		dead = true
		$death.play()
		$sprite.play("death")
		
	hits += 1
	
func add_points(value):
	var point = points.instantiate()
	randomize()
	var rx = randi_range(75, 150)
	point.position.x = position.x - rx
	point.position.y = position.y
	point.value = value
	get_tree().current_scene.add_child(point)


func _on_reset_timer_timeout():
	$sprite.play("default")


func _on_respawn_timer_timeout():
	$sprite.play("default")
	hits = 0
	dead = false
	active = false
	position.x=315
	position.y = -315
