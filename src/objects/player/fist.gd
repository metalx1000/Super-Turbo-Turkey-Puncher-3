extends Node2D

@onready var sprite = $sprite

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.fist = self
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if sprite.frame == 2:
		Global.turkey.hit()
		
	if Input.is_action_just_pressed("punch"):
		sprite.play("punch")
