
# Super-Turbo-Turkey-Puncher-3

Copyright Kris Occhipinti 2023-11-28

(https://filmsbykris.com)

License GPLv3

# Understanding this game
It's a Joke.  It's a recreation of a mini hidden game from DOOM3.

This game's Source Code is Free and Open Source under a GPLv3 license.

Please understand that the sounds, music, and sprites are not free.
They have copyrights held byt ID Software.
